import { createStore, compose, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

const enhancers = compose(
  applyMiddleware(thunk),
  window.devToolsExtension ? window.devToolsExtension() : f => f
);

const persistConfig = {
  key: 'root',
  storage,
  stateReconciler: autoMergeLevel2,
};


const defaultStore = {
  auth: {
    loggedIn: false,
  },
};

const rootPersistReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(rootPersistReducer, defaultStore, enhancers);
const persistor = persistStore(store);

export {
  store,
  persistor,
};
