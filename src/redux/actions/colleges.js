import * as api from '../../api';

export const FETCH_COLLEGES = 'FETCH_COLLEGES';
export const GET_COLLEGES = 'GET_COLLEGES';
export const FETCH_START = 'FETCH_START';
export const FETCH_END = 'FETCH_END';
export const ERROR = 'ERROR';


export const getColleges = list => ({
  type: GET_COLLEGES,
  payload: list,
});

export const raiseError = () => ({
  type: ERROR,
});

export const fetchStart = () => ({
  type: FETCH_START,
});

export const fetchEnd = () => ({
  type: FETCH_END,
});

export function fetchColleges() {
  return (dispatch) => {
    dispatch(fetchStart());
    return api.fetchColleges()
      .then((res) => {
        dispatch(getColleges(res));
        dispatch(fetchEnd());
      }, (err) => {
        console.log('Inside colleges', err);
        dispatch(raiseError());
      })
      .catch(err => console.log(err));
  };
}
