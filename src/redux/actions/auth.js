export const FETCH_START = 'FETCH_START';
export const FETCH_END = 'FETCH_END';


export const fetchStart = () => ({
  type: FETCH_START,
});

export const fetchEnd = () => ({
  type: FETCH_END,
});

export const logIn = () => ({
  type: 'LOGIN',
});

export const logOut = () => ({
  type: 'LOGOUT',
});
