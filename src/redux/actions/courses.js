import * as api from '../../api';

export const FETCH_COURSES = 'FETCH_COURSES';
export const GET_COURSES = 'GET_COURSES';
export const FETCH_START = 'FETCH_START';
export const FETCH_END = 'FETCH_END';
export const ERROR = 'ERROR';


export const getCourses = list => ({
  type: GET_COURSES,
  payload: list,
});

export const raiseError = () => ({
  type: ERROR,
});

export const fetchStart = () => ({
  type: FETCH_START,
});

export const fetchEnd = () => ({
  type: FETCH_END,
});

export function fetchCourses() {
  return (dispatch) => {
    dispatch(fetchStart());
    return api.fetchCourses()
      .then((res) => {
        dispatch(getCourses(res));
        dispatch(fetchEnd());
      }, err => (err) => {
        console.log('Inside colleges', err);
        dispatch(raiseError());
      })
      .catch(err => console.log(err));
  };
}
