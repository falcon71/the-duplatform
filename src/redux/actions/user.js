import * as api from '../../api';

export const FETCH_USER = 'FETCH_USER';
export const GET_USER = 'GET_USER';
export const FETCH_START = 'FETCH_START';
export const FETCH_END = 'FETCH_END';
export const ERROR = 'ERROR';


export const getUser = user => ({
  type: GET_USER,
  payload: user,
});

export const raiseError = () => ({
  type: ERROR,
});

export const fetchStart = () => ({
  type: FETCH_START,
});

export const fetchEnd = () => ({
  type: FETCH_END,
});

export function fetchUserByEmail(email) {
  return (dispatch) => {
    dispatch(fetchStart());
    return api.fetchUserByEmail(email)
      .then((res) => {
        dispatch(getUser(res));
        dispatch(fetchEnd());
      }, (err) => {
        console.log(err);
        dispatch(raiseError());
      });
  };
}
