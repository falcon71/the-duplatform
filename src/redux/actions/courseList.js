import * as api from '../../api';

export const FETCH_COURSE_LIST = 'FETCH_COURSE_LIST';
export const GET_COURSE_LIST = 'GET_COURSE_LIST';
export const FETCH_START = 'FETCH_START';
export const FETCH_END = 'FETCH_END';
export const ERROR = 'ERROR';


export const getCoursesList = list => ({
  type: GET_COURSE_LIST,
  payload: list,
});

export const raiseError = () => ({
  type: ERROR,
});

export const fetchStart = () => ({
  type: FETCH_START,
});

export const fetchEnd = () => ({
  type: FETCH_END,
});

export function fetchCourseList() {
  return (dispatch) => {
    dispatch(fetchStart());
    return api.fetchCourseList()
      .then((res) => {
        dispatch(getCoursesList(res));
        dispatch(fetchEnd());
      }, err => console.log(err));
  };
}
