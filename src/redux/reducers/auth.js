import { FETCH_END, FETCH_START } from '../actions/auth';

const auth = (state = {}, action) => {
  switch (action.type) {
    case 'LOGIN':
      return { ...state, loggedIn: true };
    case 'LOGOUT':
      return { ...state, loggedIn: false };
    case FETCH_START:
      return { ...state, isFetching: true };
    case FETCH_END:
      return { ...state, isFetching: false };
    default:
      return state;
  }
};

export default auth;
