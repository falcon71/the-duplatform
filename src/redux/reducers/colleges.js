import { GET_COLLEGES, ERROR, FETCH_START, FETCH_END } from '../actions/colleges';

const colleges = (state = {}, action) => {
  switch (action.type) {
    case GET_COLLEGES:
      return { ...state, list: action.payload };
    case FETCH_START:
      return { ...state, isFetching: true };
    case FETCH_END:
      return { ...state, isFetching: false };
    case ERROR:
      return { ...state, isError: true, isFetching: false };
    default: return state;
  }
};

export default colleges;
