import { combineReducers } from 'redux';
import auth from './auth';
import user from './user';
import colleges from './colleges';
import courses from './courses';
import courseList from './courseList';
import notification from './notification';

export default combineReducers({
  auth,
  user,
  colleges,
  courses,
  courseList,
  notification,
});
