import { GET_COURSE_LIST, ERROR, FETCH_START, FETCH_END } from '../actions/courseList';

const courses = (state = {}, action) => {
  switch (action.type) {
    case GET_COURSE_LIST:
      return { ...state, list: action.payload };
    case FETCH_START:
      return { ...state, isFetching: true };
    case FETCH_END:
      return { ...state, isFetching: false };
    case ERROR:
      return { ...state, isError: true, isFetching: false };
    default: return state;
  }
};

export default courses;
