import { SHOW_NOTIFICATION, HIDE_NOTIFICATION } from '../actions/notification';

const notification = (state = {}, action) => {
  switch (action.type) {
    case SHOW_NOTIFICATION:
      return { ...state, ...action.payload };
    case HIDE_NOTIFICATION:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};

export default notification;
