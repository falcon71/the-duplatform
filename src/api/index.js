import endPoints from './endpoints';

export const fetchColleges = async () => {
  const response = await fetch(endPoints.colleges);
  const responseJson = response.json();
  return responseJson;
};

export const fetchCourses = async () => {
  const response = await fetch(endPoints.courses);
  const responseJson = response.json();
  return responseJson;
};

export const fetchCourseList = async () => {
  const response = await fetch(endPoints.courseList);
  const responseJson = await response.json();
  const courseList = responseJson.courselists;
  return courseList;
};


export const fetchUserByEmail = async (email) => {
  // fetch user by email id
  const response = await fetch(`${endPoints.user}/${email}`);
  const responseJson = await response.json();
  const user = responseJson.user[0];
  // save user to app state
  return user;
};


export const saveMetaData = async (metadata, email) => {
  const body = {
    ...metadata,
    uploadedBy: email,
  };

  const response = await fetch(endPoints.file, {
    method: 'POST',
    headers: {
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  });

  return response;
};
