export default {
  colleges: 'https://falcon-theduplatform.herokuapp.com/api/colleges',
  courses: 'https://falcon-theduplatform.herokuapp.com/api/courses',
  courseList: 'https://falcon-theduplatform.herokuapp.com/api/courselist',
  user: 'https://falcon-theduplatform.herokuapp.com/api/user',
  file: 'https://falcon-theduplatform.herokuapp.com/api/file',
};
