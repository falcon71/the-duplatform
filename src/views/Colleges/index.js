import CollegeList from './CollegeList';
import CollegeDetails from './CollegeDetails';

export {
  CollegeDetails,
  CollegeList,
};
