import React from 'react';
import Paper from 'material-ui/Paper';

import './Colleges.css';

function AboutCollege(props) {
  const {
    collegename,
    address,
    contact,
    site,
    email,
  } = props.college;
  return (
    <div>
      <img
        src={require('../../images/kmv.png')}
        alt="college"
        style={{ width: '100%', padding: '0 px' }}
      />
      <Paper zDepth={2} className="container">
        <div>
          <h3 className="title">{collegename}</h3>
        </div>

        <div>
          <h3 className="sub-title">Address</h3>
          <h5 className="text">{address}</h5>
        </div>
        <div>
          <h3 className="sub-title">Contact</h3>
          <h5 className="text">{contact}</h5>
        </div>
        <div>
          <h3 className="sub-title" >Website</h3>
          <h5 className="text">
            <a
              href={`http://${site}`}
              style={{ textDecoration: 'none', color: 'inherit' }}
            >
              {site}
            </a>
          </h5>
        </div>
        <div>
          <h3 className="sub-title">Email</h3>
          <h5 className="text">{email}</h5>
        </div>
      </Paper>
    </div>
  );
}

export default AboutCollege;
