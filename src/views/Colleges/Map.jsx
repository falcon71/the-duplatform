import React, { Component } from 'react';
import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react';

const API_KEY = 'AIzaSyDtPLfuJ9zoALjpXTb9pYOuoxcUerWD0SU';

export class MapContainer extends Component {
  constructor(props) {
    super(props);
    this.state = { lat: '28.68784', lng: '77.1179923' };
  }

  render() {
    const { lat, lng } = this.state;
    return (
      <Map
        google={this.props.google}
        zoom={16}
        initialCenter={{
          lat,
          lng,
        }}
      >
        <Marker
          onClick={() => alert('There you go!')}
          name="Current location"
        />

        <InfoWindow onClose={this.onInfoWindowClose}>
          <div>{/* <h1>{this.state.selectedPlace.name}</h1> */}</div>
        </InfoWindow>
      </Map>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: API_KEY,
})(MapContainer);
