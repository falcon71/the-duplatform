import React, { Component } from "react";
import { connect } from 'react-redux';
import Paper from "material-ui/Paper";
import { List, ListItem } from "material-ui/List";
import { Link } from "react-router-dom";
import Divider from 'material-ui/Divider';
import ActionAccountBalance from "material-ui/svg-icons/action/account-balance";
import TextField from 'material-ui/TextField';
import Search from "material-ui/svg-icons/action/search";


import Loading from '../../components/Loading';
import { endPoints } from "../../config/routes";


class Colleges extends Component {
  constructor(props) {
    super(props);
    this.state = {
      colleges: this.props.colleges.sort((a, b) => (
        a.collegename > b.collegename
          ? 1
          : b.collegename > a.collegename ? -1 : 0
      )),
      refreshed: true
    };
  }
  handleSearch = event => {
    const input = event.target.value.toLowerCase();
    if (input === '') {
      this.setState({ colleges: this.props.colleges });
      return;
    }
    const colleges = this.state.colleges;
    const filteredList = colleges.filter(college => college.collegename.toLowerCase().indexOf(input) !== -1)
    this.setState({ colleges: filteredList });
  };
  render() {
    const { colleges, refreshed } = this.state;
    const { isFetching } = this.props;
    if (refreshed && !isFetching) {
      this.setState({ refreshed: false, colleges: this.props.colleges });
    }
    return (
      <div>
        <Paper
          zDepth={2}
          style={{ marginBottom: '20px' }}
        >
          <div style={{ display: 'flex', width: '90%' }}>
            <Search
              style={{
                margin: '6px',
                paddingLeft: '10px',
                minHeight: '40px',
                minWidth: '40px',
                fontSize: '20px'
              }} />
            <TextField
              hintText="Search"
              onChange={this.handleSearch}
              underlineShow={false}
              style={{ marginLeft: '20px' }}
            />
          </div>
          <Divider />
        </Paper>
        {isFetching
          ? (
            <div>
              <Loading />
            </div>
          )
          : (
            <Paper zDepth={2}>
              <List>
                {colleges.map((college, i) => (
                  <Link
                    to={`${endPoints.collegeDetails}/${college._id}`}
                    key={college._id}
                    style={{ textDecoration: "none" }}
                  >
                    <ListItem
                      primaryText={college.collegename}
                      leftIcon={<ActionAccountBalance />}
                    />
                    <Divider />
                  </Link>
                ))}
              </List>
            </Paper>
          )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  colleges: state.colleges.list.colleges,
  isFetching: state.colleges.isFetching,
});

export default connect(mapStateToProps, null)(Colleges);
