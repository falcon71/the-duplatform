import React, { Component } from "react";
import { withRouter, Route } from "react-router-dom";
import { connect } from 'react-redux';

import {
  Card,
  CardActions,
  CardHeader,
  CardMedia,
  CardTitle
} from "material-ui/Card";
import RaisedButton from "material-ui/RaisedButton";

import AboutCollege from "./About";
import Map from "./Map";
import TableView from '../../components/Table';
import { endPoints } from "../../config/routes";

class CollegeDetails extends Component {
  constructor(props) {
    super(props);
    this.state = { isMain: true, availableCourses: [], isLoading: true, college: {} };
  }

  componentDidMount() {
    const { courseList, colleges } = this.props;
    const id = this.props.match.params.id;
    const college = colleges.filter(college => college._id === id).pop();
    const result = courseList.filter(item => item.collegename === college.collegename);
    this.setState({ college, isLoading: false, availableCourses: result });
  }

  handleAbout = () => {
    const { history } = this.props;
    const { college } = this.state;
    this.setState({ isMain: false });
    history.push(`${endPoints.collegeDetails}/${college._id}/about`);
  };

  handleNearby = () => {
    const { history } = this.props;
    const { college } = this.state;

    this.setState({ isMain: false });
    history.push(`${endPoints.collegeDetails}/${college._id}/map`);
  };

  handleSeeCourses = () => {
    const { history } = this.props;
    const { college } = this.state;

    this.setState({ isMain: false });
    history.push(`${endPoints.collegeDetails}/${college._id}/allCourses`);
  }

  render() {
    const { history } = this.props;
    const { college, isMain } = this.state;
    const headers = ['Course Name', 'Gen', 'Obc', 'Sc', 'St', 'Annual Fee', 'Annual Fee PWD'];
    if ((history.location.pathname === (`${endPoints.collegeDetails}/${college._id}`)) && (this.state.isMain === false)) {
      this.setState({ isMain: true })
    }
    return (
      <div>
        {isMain && (
          <div>
            <Card>
              <CardMedia overlay={<CardTitle title={college.collegename} />}>
                <img src={require("../../images/kmv.png")} alt="College Campus" />
              </CardMedia>
            </Card>
            <Card>
              <CardHeader title="About College" style={{ color: "#01579b" }} />
              <CardActions>
                <RaisedButton
                  label="More About"
                  primary
                  onClick={this.handleAbout}
                />
              </CardActions>
            </Card>
            <Card>
              <CardHeader title="Nearby places" style={{ color: "#01579b" }} />
              <CardActions>
                <RaisedButton
                  label="More Nearby"
                  primary
                  onClick={this.handleNearby}
                />
              </CardActions>
            </Card>
            <Card>
              <CardHeader title="Course" style={{ color: "#01579b" }} />
              <CardActions>
                <RaisedButton label="See Courses" primary onClick={this.handleSeeCourses} />
              </CardActions>
            </Card>
            <Card>
              <CardHeader title="Stats" style={{ color: "#01579b" }} />
              <CardActions>
                <RaisedButton label="More Stats" primary />
              </CardActions>
            </Card>
          </div>
        )}

        <Route
          path={`${endPoints.collegeDetails}/${college._id}/about`}
          render={() => <AboutCollege college={college} />}
        />

        <Route
          path={`${endPoints.collegeDetails}/${college._id}/map`}
          render={() => <Map />}
        />

        <Route
          path={`${endPoints.collegeDetails}/${college._id}/allCourses`}
          render={() => <TableView headers={headers} list={this.state.availableCourses} forCollege={false} isLoading={this.state.isLoading} />}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  courseList: state.courseList.list,
  colleges: state.colleges.list.colleges,
});

export default withRouter(connect(mapStateToProps, null)(CollegeDetails));
