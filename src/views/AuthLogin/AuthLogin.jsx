import React, { Component } from "react";
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { TextField, Paper, RaisedButton } from "material-ui";
import FlatButton from "material-ui/FlatButton";
import { auth } from "../../firebase";
import LinearProgress from "material-ui/LinearProgress";
import Lock from 'material-ui/svg-icons/action/lock';

import { showNotification } from '../../redux/actions/notification';
import { logIn, fetchStart, fetchEnd } from '../../redux/actions/auth';
import { fetchUserByEmail } from '../../redux/actions/user';
import { endPoints } from '../../config/routes';

const Loading = () => <LinearProgress color="#ec407a" mode="indeterminate" />;

class AuthLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      emailError: "",
      passError: "",
      isLoading: false,
      color: 'white',
      opacity: 1
    };
  }

  componentDidMount() {
    this.props.fetchStart();
    auth.onAuthStateChanged(user => {
      if (user) {
        this.props.fetchUserByEmail(user.email);
        // call action handler
        this.props.logIn();
        this.props.showNotification('Successfully logged In.');
        this.props.fetchEnd();
        this.setState({ isLoading: false });
      }
    });
  }

  handleEmailChange = event => {
    this.setState({
      email: event.target.value
    });
  };

  handlePasswordChange = event => {
    this.setState({ password: event.target.value });
  };

  handleSignUp = () => {
    const { history } = this.props;
    history.push(endPoints.signup);
  };
  handleForgot = () => {
    const { email } = this.state;
    if (email.length <= 10)
      this.setState({
        emailError: "Invalid email "
      });
    else {
      auth
        .sendPasswordResetEmail(this.state.email)
        .then(function () {
          this.props.showNotification('Mail sent.');
        })
        .catch(function (error) {
        });
    }
  };
  handleLogin = () => {
    this.setState({ isLoading: true, color: '#f5f5f5', opacity: 0.6 });
    const { email, password } = this.state;

    // Sign in using firebase auth
    auth.signInWithEmailAndPassword(email, password).catch(err => {
      if (err.code === "auth/invalid-email") {
        this.setState({
          emailError: "The email address is badly formatted.",
          isLoading: false,
          color: 'white'
        });
      }
      if (err.code === "auth/user-not-found") {
        this.setState({
          emailError: "This email address is not registered with us.",
          isLoading: false,
          color: 'white',
          opacity: 1
        });
      }
      if (err.code === "auth/wrong-password") {
        this.setState({
          passError: "Invalid Password",
          isLoading: false,
          color: 'white',
          opacity: 1
        });
      }
      if (err.code === "auth/network-request-failed") {
        this.setState({
          isLoading: false,
          color: 'white',
          opacity: 1
        });
        this.props.showNotification('Network failure. Retry!');
      }
    });
  };
  render() {
    const {
      isLoading,
      email,
      password,
      emailError,
      passError,
      color,
      opacity,
    } = this.state;
    return (
      <div>
        {isLoading && <Loading />}
        {
          <Paper
            zDepth={2}
            style={{
              height: '720px  ',
              flexGrow: "1",
              flexBasis: "90%",
              padding: "10px",
              textAlign: "center",
              backgroundColor: color,
              opacity: opacity
            }}
          >
            <p>
              Welcome to <b style={{ color: "#01579b" }}>The DuPlatform </b>!
            </p>
            <TextField
              floatingLabelText="Email"
              hintText="you@example.com"
              style={{ width: "85%" }}
              value={email}
              onChange={this.handleEmailChange}
              errorText={emailError}
            />

            <TextField
              floatingLabelText="Password"
              hintText="confidential"
              style={{ width: "85%" }}
              onChange={this.handlePasswordChange}
              type="password"
              errorText={passError}
              value={password}
            />
            <br />
            <br />
            <RaisedButton
              primary
              label="Login"
              style={{ marginTop: "15px" }}
              icon={<Lock />}
              onClick={this.handleLogin}
              disabled={isLoading}
            />
            <br />
            <FlatButton
              primary
              label="Forgot Password?"
              style={{ marginTop: "10px" }}
              onClick={this.handleForgot}
              disabled={isLoading}
            />
            <br />
            <div style={{ padding: "10px 5px" }}>
              Not a member?
              <FlatButton
                primary
                label="SignUp"
                style={{ margin: "10px" }}
                onClick={this.handleSignUp}
                disabled={isLoading}
              />
            </div>
          </Paper>
        }
      </div>
    );
  }
}

export default withRouter(connect(null, { 
  logIn,
  fetchStart,
  fetchEnd,
  fetchUserByEmail,
  showNotification,
  })( AuthLogin));

AuthLogin.propTypes = {
  logIn: PropTypes.func.isRequired,
  fetchStart: PropTypes.func.isRequired,
  fetchEnd: PropTypes.func.isRequired,
  fetchUserByEmail: PropTypes.func.isRequired,
};
