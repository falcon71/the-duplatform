import React, { Component } from "react";
import { connect } from 'react-redux';
import firebase from "firebase";
import Paper from "material-ui/Paper";
import TextField from "material-ui/TextField";
import RaisedButton from "material-ui/RaisedButton";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";
import FileCloudUpload from "material-ui/svg-icons/file/cloud-upload";
import DoneAll from "material-ui/svg-icons/action/done-all";
import CustomUploadButton from 'react-firebase-file-uploader/lib/CustomUploadButton';
import Snackbar from 'material-ui/Snackbar';
import LinearProgress from 'material-ui/LinearProgress';

import * as api from '../../api';
import "../Static/common.css";
import Loading from '../../components/Loading';
import { showNotification, hideNotification } from '../../redux/actions/notification';

const SumbitLoading = () => <LinearProgress color="#ec407a" mode="indeterminate" />;

class Contribute extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filename: "",
      type: 'practical',
      useful: null,
      sem: null,
      isUploading: false,
      uploaded: false,
      progress: 0,
      downloadUrl: '',
      errorFile: '',
      errorSem: '',
      errorUseful: '',
      submitLoading: false,
      color: 'white',
      opacity: 1
    };
  }

  handleTypeChange = (event, index, type) => this.setState({ type });
  handleCourseChange = (event, index, useful) => this.setState({ useful });
  handleSemChange = (event, index, sem) => this.setState({ sem });
  handleFilenameChange = (event) => this.setState({ filename: event.target.value });

  handleUploadStart = () => this.setState({ isUploading: true, progress: 0 });
  handleProgress = (progress) => this.setState({ progress });
  handleUploadError = (error) => {
    this.setState({ isUploading: false, snackbar: true, message: error });
    console.error(error);
  }
  handleUploadSuccess = (filename) => {
    this.setState({ filename, progress: 100, isUploading: false, uploaded: true });
    firebase.storage()
      .ref(this.state.type)
      .child(filename)
      .getDownloadURL()
      .then(url => this.setState({ downloadUrl: url }));
  };

  handleSubmit = () => {
    const { filename, downloadUrl, sem, type, useful } = this.state;
    const { email } = this.props;
    if (filename === '')
      this.setState({
        errorFile: 'Filename cannot be empty'
      });
    else if (sem === null)
      this.setState({
        errorSem: 'Should select a semester '
      });
    else if (useful === null)
      this.setState({
        errorUseful: 'Should select one course'
      });
    else {
      this.setState({ submitLoading: true, color: '#f5f5f5', opacity: 0.6 });
      console.log('meta data', this.state);
      const metaDate = {
        name: filename,
        url: downloadUrl,
        useful,
        type,
        sem
      };
      api.saveMetaData(metaDate, email)
        .then(response => {
          if (response.status !== 200) {
            this.props.showNotification('Could Not process your request');
            this.props.history.push('/');
          }
          else {
            this.props.showNotification('Successfully Uploaded');            
            this.props.history.push('/resources');
          }
        })
    }
  }

  render() {
    const { courses, isLoading } = this.props;
    const { sem, type, useful, submitLoading, color, filename, opacity, errorFile, progress, snackbar, message, errorSem, errorUseful } = this.state;
    const sems = [1, 2, 3, 4, 5, 6, 7, 8];
    return (
      <div>
        {submitLoading && <SumbitLoading />}
        {isLoading && <Loading />}
        {!isLoading && (
          <Paper
            style={{ padding: "20px", textAlign: "center", height: '736px', backgroundColor: color, opacity: opacity }}
          >
            <div
              className="title"
              style={{ textAlign: "center" }}
            >
              Contribute to the Society
            </div>
            <TextField
              floatingLabelText="Material Name"
              style={styles.width}
              value={filename}
              errorText={errorFile}
              onChange={this.handleFilenameChange}
            />
            <SelectField
              floatingLabelText="Material Type"
              value={this.state.type}
              onChange={this.handleTypeChange}
              style={styles.width}
              floatingLabelStyle={{ color: "#01579b" }}
            >
              <MenuItem value={"ebooks"} primaryText="E-Book" />
              <MenuItem value={"notes"} primaryText="Notes" />
              <MenuItem
                value={"papers"}
                primaryText="Previous Year Papers"
              />
              <MenuItem value={"sample-papers"} primaryText="Sample Paper" />
              <MenuItem value={"practicals"} primaryText="Practicals" />
            </SelectField>
            <SelectField
              floatingLabelText="Useful for "
              value={useful}
              onChange={this.handleCourseChange}
              style={styles.width}
              errorText={errorUseful}
              floatingLabelStyle={{ color: "#01579b" }}
            >
              {courses.map(course => (
                <MenuItem
                  key={course._id}
                  value={course.coursename}
                  primaryText={course.coursename}
                />
              ))}
            </SelectField>
            <SelectField
              floatingLabelText="Semester"
              value={sem}
              errorText={errorSem}
              onChange={this.handleSemChange}
              style={styles.width}
              floatingLabelStyle={{ color: "#01579b" }}
            >
              {sems.map(sem => (
                <MenuItem
                  key={sem}
                  value={sem}
                  primaryText={sem}
                />
              ))}
            </SelectField>
            <br />
            <br />
            <CustomUploadButton
              accept=".pdf,application/zip"
              name="file"
              filename={filename}
              storageRef={firebase.storage().ref(type)}
              onUploadStart={this.handleUploadStart}
              onUploadError={this.handleUploadError}
              onUploadSuccess={this.handleUploadSuccess}
              onProgress={this.handleProgress}
              style={{ backgroundColor: '#01579b', color: 'white', padding: 5, borderRadius: 4, width: '120px', height: '36px', }}
            >
              <p>Upload</p>
            </CustomUploadButton>
            {this.state.isUploading &&
              <div>
                Uploading . . .
                <LinearProgress mode="determinate" value={progress} />
              </div>
            }
            {this.state.uploaded &&
              <div style={{ display: 'flex', alignItems: 'center', flexDirection: 'row' }}>
                <FileCloudUpload
                  style={{ margin: '30px', color: '#43a047', minHeight: '40px', minWidth: '40px' }}
                />
                File Uploaded Successfully
              </div>
            }
            <RaisedButton
              icon={<DoneAll />}
              label="Submit"
              primary
              style={{ width: "120px", margin: "10px" }}
              onClick={this.handleSubmit}
              disabled={!this.state.uploaded || submitLoading}
              disableTouchRipple={false}
            />
            <Snackbar
              open={snackbar}
              message={message}
              autoHideDuration={4000}
              onRequestClose={this.handleRequestClose}
              style={{ width: '100%' }}
            />
          </Paper>
        )}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  courses: state.courses.list.courses,
  email: state.user.info.email,
});

export default connect(mapStateToProps, { showNotification, hideNotification })(Contribute);

const styles = {
  width: {
    width: "80%"
  }
};
