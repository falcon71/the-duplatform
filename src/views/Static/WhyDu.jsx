import React from 'react';
import { Paper } from 'material-ui';
import './common.css';

function WhyDu() {
  return (
    <Paper style={{ padding: '10px' }}>
      <div className="title" style={{ margin: '10px' }}>
        What's So Good About DU ?
      </div>
      <p style={{ margin: '10px 12px', textAlign: 'justify' }}>
        Since its establishment in 1922, the
        <i style={{ color: '#01579b' }}>University of Delhi</i> (DU) has
        remained at the forefront of higher education and continues to be one
        of the finest centres of higher learning globally. DU comprises 70
        colleges, 15 faculties, 87 departments and 11 centres and campuses
        spread across Delhi -- the nation’s capital. We admit over 60,000
        undergraduate and 8,000 post-graduate students annually besides over
        250,000 students to the School of Open Learning.
      </p>
      <p style={{ margin: '10px 12px', textAlign: 'justify' }}>

        Endowed with a vast pool of intellectual resources in diverse fields
        of learning, the University is a proud alma mater of many alumini who
        have distinguished themselves in different spheres of human endeavour,
        including many Heads of State.
        <i style={{ color: '#01579b' }}>

          Presidents of four countries have graduated from our University; one
          Prime Minister of India
        </i>also graduated from DU and another decorated its faculty. Some of
India’s best known litterateurs, intellectuals, scientists,
economists, legal luminaries, civil servants, defence personnel,
politicians, sports persons, film personalities and business leaders
have been students of the University of Delhi or have served as its
faculty members.
      </p>
      <p style={{ margin: '10px 12px', textAlign: 'justify' }}>
        In a world that is presently experiencing unprecedented challenges, we
        are confident that the values reflected in the University’s motto -
        <i style={{ color: '#01579b' }}>Nishtha Dhriti Satyam</i> (firm
devotion to truth) -- shall stand us in good stead to rededicate
ourselves to the philosophy of universal brotherhood. Upholding our
core cultural philosophy of
        <i style={{ color: '#01579b' }}>Vasudhaiva Kutumbakam</i> (the world
is one family), the University is committed to nation building with
firm adherence to universal values. DU recognize that the primary role
of an educational institution is to channelize the energies of youth
toward productive and creative goals through an unfettered pursuit of
knowledge.
      </p>
      <p style={{ margin: '10px 12px', textAlign: 'justify' }}>
        <i style={{ color: '#01579b' }}>University of Delhi</i> is an
        important global leader in knowledge creation and dissemination. We
        remain devoted to educating future leaders of the 21st century through
        the transformative power of liberal arts, social sciences and cutting
        edge science and technology education. The University believes in
        building and sustaining an academic ambience that enables students
        realize their potential without fear or favour. Recognizing the
        academic aspirations of international students to be educated at DU,
        almost five per cent of the seats are earmarked for foreign students.
        We welcome students from all over the world, especially from Africa,
        Asia and Latin America.
      </p>
      <p style={{ margin: '10px 12px', textAlign: 'justify' }}>

        The university encourages to participate and engage in a diverse and
        supportive community that nurtures creativity and independent
        thinking. The University offers world-class education that makes our
        students highly sought after applicants by various employers and the
        centres of higher learning and research.The university stands out for
        its exceptional educational opportunities straddling a wide range of
        programmes, advanced curriculum, constructive pedagogy, renowned
        faculty, comprehensive extracurricular activities, and an excellent
        infrastructure.
      </p>
      <p style={{ margin: '10px 12px', textAlign: 'justify' }}>

        To promote critical thinking and relevant soft skills in the students,
        university provides ample scope for undergraduate research and skill
        development through a wide range of innovation projects. The learnings
        from these projects enable our students to find gainful employment in
        the market or pursue higher education at institutions of their choice.
        These institutional attributes enable our students acquire various
        skill sets and the intellect that attracts prestigious employers to
        recruit a large number of our students through the Central Placement
        Cell of the University.Firmly believing that equity and excellence are
        mutually linked, university is committed to enrolling a significant
        proportion of students from disadvantaged socio-economic sections of
        society. To ensure that no potential student is deterred from seeking
        admission to this University due to lack of resources, the university
        offers support to deserving students by way of financial assistance.
      </p>
      <p style={{ margin: '10px 12px', textAlign: 'justify' }}>

        University of Delhi is committed to gender sensitization and women’s
        empowerment, and ensures security of our students through a range of
        regulatory policies and practices. Equally, the University stands firm
        on its anti- ragging policy and expects cooperation from its students
        in maintaining a non-threatening and purposeful atmosphere at its
        campuses and colleges. The University provides a barrier-free
        environment for the independence, convenience and safety of all our
        students including the differently-abled.
      </p>
    </Paper>
  );
}

export default WhyDu;
