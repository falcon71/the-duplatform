import React from 'react';
import Paper from 'material-ui/Paper';

import './common.css';

const styles = {
  container: {
    padding: '20px',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
};

function Admissions() {
  return (
    <Paper style={styles.container}>
      <h3 className="title"> Documents required at the time of Admission</h3>
      <ol>
        <li>Class X Board Examination Certificate</li>
        <li>Class X Mark-Sheet</li>
        <li>Class XII Mark-Sheet</li>
        <li>Class XII Provisional Certificate / Original Certificate</li>
        <li>Recent Character Certificate</li>
        <li>
          SC/ST/PwD/CW/KM Certificate (in the name of the Applicant) issued by
          the competent authority
        </li>
        <li>
          OBC (Non-Creamy Layer) Certificate (in the name of the Applicant) as
          in central list
        </li>
        <li>
          Transfer Certificate from school / college as well as Migration
          Certificate from Board / University are required from those students
          who have passed senior secondary exam from outside Delhi
        </li>
        <li>At least two passport size self-attested photographs.</li>
      </ol>

      <h3 className="title">
        {' '}
        Admissions to various colleges of the University of Delhi are based on
        merit/Entrance criteria.
      </h3>
      <ol style={{ textAlign: 'justify' }}>
        <li>
          Applicants seeking admission to UG merit based programmes/courses
          must register on a centralized admission Web Portal, for which the
          details are given in this Bulletin.
        </li>
        <li>
          {' '}
          Applicants seeking admission to UG entrance test based
          programme/courses need to apply on a separate portal for which the
          registration will start soon and date will be announced on the
          <i style={{ color: '#01579b' }}>
            <a
              target="_blank"
              href="du.ac.in"
              style={{ textDecoration: 'none', color: 'inherit' }}
            >
              {' '}
              University of Delhi website
            </a>
          </i>
        </li>
      </ol>
      <br />
      <h3 className="title">UG Merit Based Admission procedure</h3>
      <ol style={{ textAlign: 'justify' }}>
        <li>
          <b>Online Registration :</b> The applicants seeking admission to UG
            merit based programme must register through online registration{' '}
        </li>
        <li>
          <b>Cut-offs:</b> The applicants must check the centralized
          <i style={{ color: '#01579b' }}>
            <a
              target="_blank"
              href="du.ac.in"
              style={{ textDecoration: 'none', color: 'inherit' }}
            >
              {' '}
              Admission Web Portal{' '}
            </a>
          </i>
          and respective college website for cut-offs for different
          programmes.
        </li>
        <li>
          <b>Selection of Colleges/Programmes:</b>The applicants who meet the
          requisite cut-off should login to the UG admission portal into their
          registration account and select college/programme where the
          applicant wishes to take admission and meets the desired cut- off
          criterion.
        </li>
        <li>
          <b>Verification of documents at Colleges:</b> The applicant shall
          take the print out of the admission form and together with the list
          of documents/certificates mentioned above, and proceed to the
          respective college for verification of mark-sheet/certificates,
          calculation of cut-off percentage depending on the programme/course.
        </li>
        <li>
          <b> Approval of admission:</b> The college will retain the
          certificates of approved applicant in order to avoid multiple
          admissions. The certificates will remain with the colleges during
          the admission process. After this, the college will approve the
          admission on the UG admission portal. However, the colleges shall
          promptly return the documents in case student withdraws/cancels the
          admission or student wishes to appear in counselling of any other
          university/ institute.
        </li>
        <li>
          <b>Online payment of Admission fee:</b> The applicant will then be
          required to log-on to the UG admission portal to make the online
          Admission Fee payment through the available online payment options.
          The approved applicant is permitted to make online admission fee
          payment till 12 noon of the next day of the given admission list.
        </li>
        <p>
          <b>
            {' '}
            Note: Students are advised to keep this fee slip on your drive
            because you will be asked at many places many times this slip.
          </b>
        </p>
      </ol>
    </Paper >
  );
}

export default Admissions;
