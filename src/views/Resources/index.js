import Resources from './Resources';
import DownloadList from './DownloadList';

export {
  Resources,
  DownloadList,
};
