import React from 'react';
import { withRouter } from 'react-router-dom';
import { GridList, GridTile } from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: '95%',
    height: '100%',
    overflowY: 'auto',
  },
};
const tilesData = [
  {
    img: require('../../assets/books.jpg'),
    path: 'ebooks',
    title: 'E-Books',
  },
  {
    img: require('../../assets/syll.png'),
    path: 'practicals',
    title: 'Practicals',
  },
  {
    img: require('../../assets/pyp.png'),
    path: 'papers',
    title: 'Past Years',
  },
  {
    img: require('../../assets/notes.jpeg'),
    path: 'notes',
    title: 'Notes',
  },
];

/**
 * A simple example of a scrollable `GridList` containing a [Subheader](/#/components/subheader).
 */
function Resources(props) {
  return (
    <div style={styles.root}>
      <GridList cellHeight={250} style={styles.gridList}>
        <Subheader
          style={{
            fontSize: '28px',
            color: '#01579b',
            margin: '20px',
            textAlign: 'center',
          }}
        >
          Resources
        </Subheader>
        {tilesData.map(tile => (
          <GridTile
            key={tile.img}
            title={tile.title}
            onClick={() => props.history.push(`/resources/${tile.path}`)}
            actionIcon={
              <IconButton>
                <StarBorder color="#fff" />
              </IconButton>
            }
          >
            <img src={tile.img} alt={tile.title} />
          </GridTile>
        ))}
      </GridList>
    </div>
  );

}

export default withRouter(Resources);
