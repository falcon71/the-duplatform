import React, { Component } from 'react';
import { connect } from 'react-redux';
import Paper from 'material-ui/Paper';
import { List, ListItem } from 'material-ui/List';
import DownloadIcon from 'material-ui/svg-icons/file/file-download';
import Divider from 'material-ui/Divider';
import Loading from '../../components/Loading';

class DownloadList extends Component {
  constructor(props) {
    super(props);
    this.state = { files: [] };
  }

  async componentWillMount() {
    const { course, location: { pathname } } = this.props;
    const fileType = pathname.split('/').pop();
    // set api
    const api = `https://falcon-theduplatform.herokuapp.com/api/file/${course}`;
    const response = await fetch(api);
    const responseJson = await response.json();
    const files = responseJson.file;
    
    const filteredFiles = files.filter(file => file.type === fileType);
    this.setState({ files: filteredFiles });
  }

  render() {
    const { files } = this.state;
    return (
      <Paper>
        <h3 style={{ color: '#01579b', padding: '10px' }} >
          Files that might help you
        </h3>
        <Divider />
        {
          files.length ?
            <List>
              {files.map(file =>
                (
                  <a
                    key={file._id}
                    href={file.url}
                    target="blank"
                    style={{ textDecoration: 'none' }}
                  >
                    <ListItem
                      leftIcon={<DownloadIcon />}
                      primaryText={file.name}
                    />
                    <Divider />
                  </a>))
              }
            </List>
            :
            <Loading />
        }
      </Paper>
    );
  }
}

const mapStateToProps = state => ({
  course: state.user.info.course,
});

export default connect(mapStateToProps, null)(DownloadList);
