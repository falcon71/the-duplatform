import React, { Component } from "react";
import { withRouter, Route } from "react-router-dom";
import { connect } from 'react-redux';
import {
  Card,
  CardActions,
  CardHeader,
  CardMedia,
  CardTitle
} from 'material-ui/Card';
import RaisedButton from "material-ui/RaisedButton";
import { endPoints } from "../../config/routes";
import Subjects from "./Subject";
import TableView from "../../components/Table";

class CourseDetails extends Component {
  constructor(props) {
    super(props);
    this.state = { isMain: true, colleges: [], isLoading: true, course: {} };
  }

  componentDidMount() {
    const { courseList, courses } = this.props;
    const id = this.props.match.params.id;
    const course = courses.filter(course => course._id === id).pop();
    const result = courseList.filter(item => item.coursename === course.coursename);
    this.setState({ course, isLoading: false, colleges: result });
  }

  handleSubject = () => {
    const { history } = this.props;
    const { course } = this.state;
    this.setState({ isMain: false });
    history.push(`${endPoints.courseDetails}/${course._id}/subjects`);
  }

  handleColleges = () => {
    const { history } = this.props;
    const { course } = this.state;
    this.setState({ isMain: false });
    history.push(`${endPoints.courseDetails}/${course._id}/colleges`);
  }

  render() {
    const { history } = this.props;
    const { colleges, course } = this.state;
    const headers = ['College Name', 'Gen', 'Obc', 'Sc', 'St', 'Annual Fee', 'Annual Fee PWD'];
    if (
      (history.location.pathname === (`${endPoints.courseDetails}/${course._id}`))
      && (this.state.isMain === false)) {
      this.setState({ isMain: true })
    }
    return (
      <div>
        {this.state.isMain && (
          <div>
            <Card>
              <CardMedia overlay={<CardTitle title={course.coursename} />}>
                <img src={require("../../images/kmv.png")} alt="College Campus" />
              </CardMedia>
            </Card>
            <Card>
              <CardHeader title="Best 4 Subject" style={{ color: "#01579b" }} />
              <CardActions>
                <RaisedButton
                  label="Get Best 4"
                  primary
                  onClick={this.handleSubject}
                />
              </CardActions>
            </Card>
            <Card>
              <CardHeader title="Syllabus" style={{ color: "#01579b" }} />
              <CardActions>
                <a href={course.syllabus} target="blank">
                  <RaisedButton label="Download" primary />
                </a>
              </CardActions>
            </Card>
            <Card>
              <CardHeader
                title="Colleges Available"
                style={{ color: "#01579b" }}
              />
              <CardActions>
                <RaisedButton
                  label="See Colleges"
                  primary
                  onClick={this.handleColleges}
                />
              </CardActions>
            </Card>
          </div>
        )}

        <Route
          path={`${endPoints.courseDetails}/${course._id}/subjects`}
          render={() => (
            <Subjects
              subjects={[course.sub1, course.sub2, course.sub3, course.sub4]}
            />
          )}
        />
        <Route
          path={`${endPoints.courseDetails}/${course._id}/colleges`}
          render={() => (
            <TableView headers={headers} list={colleges} forCollege={true} isLoading={this.state.isLoading} />
          )}
        />

      </div>
    );
  }
}

const mapStateToProps = state => ({
  courseList: state.courseList.list,
  courses: state.courses.list.courses,
});

export default withRouter(connect(mapStateToProps, null)(CourseDetails));
