import React, { Component } from "react";
import { connect } from 'react-redux';
import Paper from "material-ui/Paper";
import List from "material-ui/List";
import ListItem from "material-ui/List/ListItem";
import AVLibraryBook from "material-ui/svg-icons/av/library-books";
import Search from "material-ui/svg-icons/action/search";
import { Link } from "react-router-dom";
import { endPoints } from "../../config/routes";
import { TextField, Divider } from "material-ui";

import Loading from '../../components/Loading';

class Courses extends Component {
  constructor(props) {
    super(props);
    this.state = {
      courses: this.props.courses.sort((a, b) => (
        a.coursename > b.coursename
          ? 1
          : b.coursename > a.coursename ? -1 : 0
      )),
      refreshed: true
    };
  }

  handleSearch = event => {
    const input = event.target.value.toLowerCase();
    if (input === '') {
      this.setState({ courses: this.props.courses });
      return;
    }
    const courses = this.state.courses;
    const filteredList = courses.filter(course => course.coursename.toLowerCase().indexOf(input) !== -1)
    this.setState({ courses: filteredList });
  };

  render() {
    const { courses, refreshed } = this.state;
    const { isFetching } = this.props;
    if (refreshed && !isFetching) {
      this.setState({ refreshed: false, courses: this.props.courses });
    }

    return (
      <div>
        <Paper
          zDepth={2}
          style={{ marginBottom: '20px' }}
        >
          <div style={{ display: 'flex', width: '90%' }}>
            <Search
              style={{
                margin: '6px',
                paddingLeft: '10px',
                minHeight: '40px',
                minWidth: '40px',
                fontSize: '20px'
              }} />
            <TextField
              hintText="Search"
              onChange={this.handleSearch}
              underlineShow={false}
              style={{ marginLeft: '20px' }}
            />
          </div>
          <Divider />
        </Paper>
        {isFetching && <Loading />}
        {!isFetching && (
          <List>
            {
              courses.map(course => (
                <Link
                  to={`${endPoints.courseDetails}/${course._id}`}
                  key={course._id}
                  style={{ textDecoration: "none" }}
                >
                  <ListItem
                    insetChildren={true}
                    primaryText={course.coursename}
                    leftIcon={<AVLibraryBook />}
                  />
                  <Divider />
                </Link>
              ))}
          </List>
        )
        }
      </div>
    );
  }
}
const mapStateToProps = state => ({
  courses: state.courses.list.courses,
  isFetching: state.courses.isFetching,
});

export default connect(mapStateToProps, null)(Courses);
