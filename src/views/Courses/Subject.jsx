import React from 'react';
import { List, ListItem } from 'material-ui/List';
import Subject from 'material-ui/svg-icons/action/book';
import Lists from 'material-ui/svg-icons/av/library-books';
import Divider from 'material-ui/Divider';


function Subjects(props) {
  const LIST_A = ['Arabic', 'Bengali', 'Botany', 'Biology',
    'Biotechnology', 'Chemistry', 'Commerce / Business Studies',
    'Computer Science / Informatics Practices', 'Economics', 'English',
    'French', 'Geography', 'Italian', 'Legal Studies', 'Mathematics', 'Punjabi', 'Psychology', 'Sanskrit',
    'Geology', 'Music', 'Sociology', 'German', 'Persian', 'Spanish',
    'Hindi', 'Philosophy', 'Statistics', 'History', 'Physics', 'Urdu',
    'Home Science', 'Political Science', 'Zoology', 'Accountancy'];
  const LIST_C1 = ['English', 'Hindi', 'Mathematics', 'Economics', 'Business Studies', 'Commerce', 'Accountancy'];
  const LIST_C2 = ['Botany', 'Biology',
    'Biotechnology', 'Chemistry', 'Computer Science / Informatics Practices', 'Geography', 'Legal Studies', 'Business Mathematics', 'Psychology', 'Geology', 'Philosophy', 'Statistics', 'History', 'Physics', 'Home Science', 'Political Science', 'Zoology'];
  const { subjects } = props;

  return (
    <div style={{
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between',
      height: '500px',
    }}
    >
      <List>
        {subjects.map((subject, i) => (
          <ListItem
            key={i}
            leftIcon={<Subject />}
            primaryText={`${subject}       as Subject ${(i + 1)}`}
          />))
        }
      </List>
      <Divider />
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;For reference see Lists
      <List>
        <ListItem
          key="A"
          primaryText="LIST A"
          leftIcon={<Lists />}
          primaryTogglesNestedList
          nestedItems={[LIST_A.map((item, i) => <ListItem key={i} primaryText={item} />)]}
        />
        <ListItem
          key="B"
          primaryText="LIST C1"
          leftIcon={<Lists />}
          primaryTogglesNestedList
          nestedItems={[LIST_C1.map((item, i) => <ListItem key={i} primaryText={item} />)]}
        />
        <ListItem
          key="C"
          primaryText="LIST C2"
          leftIcon={<Lists />}
          primaryTogglesNestedList
          nestedItems={[LIST_C2.map((item, i) => <ListItem key={i} primaryText={item} />)]}
        />
      </List>
    </div >
  );
}

export default Subjects;
