import React, { Component } from "react";
import { auth } from "../../../firebase";
import {
  Link,
  withRouter,
  Route
} from "react-router-dom";
import { renderRoutes } from 'react-router-config';
import { connect } from 'react-redux';

import Dialog from "material-ui/Dialog";
import Snackbar from "material-ui/Snackbar";
import AppBar from "material-ui/AppBar";
import FlatButton from "material-ui/FlatButton";
import getMuiTheme from "material-ui/styles/getMuiTheme";
import Drawer from "material-ui/Drawer";
import Avatar from "material-ui/Avatar";
import ListItem from "material-ui/List/ListItem";
import ActionHome from "material-ui/svg-icons/action/home";
import ActionAssignment from "material-ui/svg-icons/action/assignment";
import SocialSchool from "material-ui/svg-icons/social/school";
import ActionAccountBalance from "material-ui/svg-icons/action/account-balance";
import ActionPermIdentity from "material-ui/svg-icons/action/perm-identity";
import ActionPowerSettings from "material-ui/svg-icons/action/power-settings-new";
import AVLibraryBook from "material-ui/svg-icons/av/library-books";
import FileCloudUpload from "material-ui/svg-icons/file/cloud-upload";
import FileCloudCircle from "material-ui/svg-icons/file/cloud-circle";
import Feedback from 'material-ui/svg-icons/action/feedback';
import HomeIcon from "material-ui/svg-icons/content/weekend";
import { showNotification } from '../../../redux/actions/notification';
import { endPoints } from "../../../config/routes";
import { logOut } from '../../../redux/actions/auth';

class HeaderWithDrawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      loggedIn: false,
      mail: false,
      disclaimer: false,
      colleges: [],
      courses: [],
      loadingColleges: true,
      loadingCourses: true,
      tologin: false,
      pressSignIn: false,
      error: false,
      user: null
    };
  }

  componentDidCatch(error) {
    console.log('Catch error')
    console.log(error);
    this.setState({ error: true });
  }

  routeToSignIn = () => {
    this.setState({ pressSignIn: true });
    this.props.history.push('/login');
  };

  handleAll = () => {
    this.props.showNotification('You must login to access this route');
  };

  handleSignIn = () => {
    this.setState({ loggedIn: true });
  };
  handleSignOut = () => {
    auth.signOut();
    this.setState({
      loggedIn: false,
      pressSignIn: false
    });
    this.props.showNotification('Successfully Logged Out!');
    this.props.logOut();
  };
  handleToggle = () => this.setState({ open: !this.state.open });
  handleClose = () => this.setState({ open: false });
  handleBoth = () =>
    this.setState({ mail: !this.state.mail, open: !this.state.open });
  handleClosemail = () => this.setState({ mail: false });
  handledisclaimer = () =>
    this.setState({
      disclaimer: !this.state.disclaimer,
      open: !this.state.open
    });
  closedisclaimer = () => this.setState({ disclaimer: false });
  toggleError = () => this.setState({ error: false })

  saveUser = async (email) => {
    // do some async task
    const api = `https://falcon-theduplatform.herokuapp.com/api/user/${email}`;
    // fetch user by email id
    const response = await fetch(api);
    const responseJson = await response.json();
    const user = responseJson.user[0];
    // save user to app state
    this.setState({ user });
  }

  hadnleAddFile = async (metadata) => {
    const email = this.state.user.email;
    const api = `https://falcon-theduplatform.herokuapp.com/api/file`;
    const body = {
      ...metadata,
      uploadedBy: email
    }
    const response = await fetch(api, {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(body)
    });
    if (response.status !== 200) {
      this.props.showNotification('Could Not process your request');
      this.props.history.push('/');
    }
    else {
      this.setState({ snackbar: true, message: 'Successfully Uploaded' });
      this.props.showNotification('Successfully Uploaded');      
      this.props.history.push('/resources');
    }
  }


  render() {
    const {
      colleges,
      courses,
      loadingColleges,
      loadingCourses,
      error,
    } = this.state;
    const { route, loggedIn, user } = this.props;
    return (
      <div>
        <AppBar
          title="The DuPlatform"
          onLeftIconButtonClick={this.handleToggle}
          iconElementRight={
            loggedIn ? (
              <FlatButton
                icon={<ActionPowerSettings onClick={this.handleSignOut} />}
              />
            ) : (
                <FlatButton
                  icon={<ActionPermIdentity onClick={this.routeToSignIn} />}
                />
              )
          }
        />
        <Drawer
          docked={false}
          width={240}
          open={this.state.open}
          onRequestChange={open => this.setState({ open })}
          primary
        >
          {loggedIn ? (
            <Link to={endPoints.default} style={styles.link}>
              <ListItem onClick={this.handleToggle}>
                <Avatar backgroundColor="#f55">{user ? user.name[0].toUpperCase() : 'A'}</Avatar>
                &nbsp; &nbsp;
                {
                  user
                    ? user.name[0].toUpperCase().concat(user.name.slice(1))
                    : 'Admin'
                }
              </ListItem>
            </Link>
          ) : (
              <ListItem onClick={this.handleToggle} leftIcon={<HomeIcon />}>
                Welcome
                </ListItem>
            )}

          <Link to={endPoints.default} style={styles.link}>
            <ListItem
              primaryText="Home"
              onClick={this.handleToggle}
              leftIcon={<ActionHome />}
            />
          </Link>
          <Link to={endPoints.whydu} style={styles.link}>
            <ListItem
              primaryText="Why DU?"
              onClick={this.handleToggle}
              leftIcon={<SocialSchool />}
            />
          </Link>
          <Link to={endPoints.admission} style={styles.link}>
            <ListItem
              primaryText="Admission"
              onClick={this.handleToggle}
              leftIcon={<ActionAssignment />}
            />
          </Link>
          <Link to={endPoints.colleges} style={styles.link}>
            <ListItem
              primaryText="Colleges"
              onClick={this.handleToggle}
              leftIcon={<ActionAccountBalance />}
            />
          </Link>
          <Link to={endPoints.courses} style={styles.link}>
            <ListItem
              primaryText="Courses"
              onClick={this.handleToggle}
              leftIcon={<AVLibraryBook />}
            />
          </Link>
          {loggedIn ? (
            <Link to={endPoints.contribute} style={styles.link}>
              <ListItem
                primaryText="Contribute"
                onClick={this.handleToggle}
                leftIcon={<FileCloudUpload />}
              />
            </Link>
          ) : (
              <ListItem
                primaryText="Contribute"
                onClick={this.handleAll}
                leftIcon={<FileCloudUpload />}
              />
            )}
          {loggedIn ? (
            <Link to={endPoints.resources} style={styles.link}>
              <ListItem
                primaryText="Resources"
                onClick={this.handleToggle}
                leftIcon={<FileCloudCircle />}
              />
            </Link>
          ) : (
              <ListItem
                primaryText="Resources"
                onClick={this.handleAll}
                leftIcon={<FileCloudCircle />}
              />
            )}
          <a
            href={'https://goo.gl/forms/5dh4hdlsRLWYGXaN2'}
            target="blank"
            style={styles.link}
          >
            <ListItem
              primaryText="FeedBack"
              onClick={this.handleToggle}
              leftIcon={<Feedback />
              }
            />
          </a>
          <ListItem
            primaryText="Mail Us"
            onClick={this.handleBoth}
            leftIcon={
              <img
                src={require("../../../assets/gmail.svg")}
                alt="gmail"
                style={{
                  maxWidth: "30px",
                  maxHeight: "30px",
                  padding: "0px",
                  marginRight: "10px"
                }}
              />
            }
          />
          <a
            href={"https://www.facebook.com/theduplatform"}
            target="blank"
            style={styles.link}
          >
            <ListItem
              primaryText="Follow us"
              onClick={this.handleToggle}
              leftIcon={
                <img
                  src={require("../../../assets/fb.svg")}
                  alt="facebook"
                  style={{
                    maxWidth: "30px",
                    maxHeight: "30px",
                    padding: "0px",
                    marginRight: "10px"
                  }}
                />
              }
            />
          </a>
          <a
            href={"https://www.linkedin.com/in/itsanshuman"}
            target="blank"
            style={styles.link}
          >
            <ListItem
              primaryText="About Creator"
              onClick={this.handleToggle}
              leftIcon={
                <img
                  src={require("../../../assets/lk.svg")}
                  alt="linkdIn"
                  style={{
                    maxWidth: "30px",
                    maxHeight: "30px",
                    padding: "0px",
                    marginRight: "10px"
                  }}
                />
              }
            />
          </a>
          <ListItem
            onClick={this.handledisclaimer}
            primaryText=" © 2018 Copyright"
            leftIcon={
              <img
                src={require("../../../assets/blueSquare.png")}
                alt="Logo"
                style={{
                  maxWidth: "30px",
                  maxHeight: "30px",
                  padding: "0px",
                  marginRight: "10px"
                }}
              />
            }
          />
        </Drawer>
        {/* Dialog box for Mail us */}
        <Dialog
          title="Mail us"
          actions={
            <FlatButton
              label="Got It!"
              primary={true}
              onClick={this.handleClosemail}
            />
          }
          modal={false}
          open={this.state.mail}
          onRequestClose={this.handleBoth}
        >
          You can mail us any time for suggestions and help at
          theduplatform@gmail.com
            </Dialog>
        {/* Dialog box for Disclaimer */}
        <Dialog
          title="Disclaimer"
          actions={
            <FlatButton
              label="Got It!"
              primary={true}
              onClick={this.closedisclaimer}
            />
          }
          modal={false}
          open={this.state.disclaimer}
          onRequestClose={this.handledisclaimer}
        >
          The Duplatform &#169; do not claim that any information provided
          by us is 100% Official and should be taken for granted. All rights
          Reserved &#174;. We are here to help the future. Any suggestions
          are welcomed.
            </Dialog>
        {renderRoutes(route.routes)}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loggedIn: state.auth.loggedIn,
  user: state.user.info, 
})

export default connect(mapStateToProps, { logOut, showNotification })(HeaderWithDrawer);

const styles = {
  link: { textDecoration: "none" }
};
