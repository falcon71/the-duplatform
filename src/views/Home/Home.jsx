import React, { Component } from 'react';
import { List, ListItem } from 'material-ui/List';
import Paper from 'material-ui/Paper';
import Avatar from 'material-ui/Avatar';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import HomeLoading from './HomeLoading';
import './Home';

const styles = {
  title: {
    fontSize: '20px',
    color: '#01579b',
    textAlign: 'center',
    padding: '10px',
  },
};

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      colleges: [
        'Keshav Mahvidyalaya',
        'Hindu College',
        'Hansraj College',
        'Ramjas College',
        'Miranda House',
        'Shri Ram College of Commerce',
        'Kirorimal College',
        'Lady Shri Ram College',
        'Shyama Prasad Mukherji College',
      ],
      colors: ['#00C851', '#ff4444', '#ffbb33', '#039be5', '#ff6d00', '#01579b'],
    };
  }
  render() {
    const { colors, colleges } = this.state;
    const { isLoading } = this.props;
    return (
      <div >
        {isLoading && <HomeLoading />}
        {!isLoading && (
          <Paper
            zDepth={2}
            style={{ height: '700px' }}
          >
            <div style={styles.title}>Trending Colleges</div>
            <List>
              {colleges.map((college, i) => (
                <ListItem
                  key={i}
                  leftAvatar={
                    <Avatar backgroundColor={colors[i % colors.length]}>
                      {i + 1}
                    </Avatar>
                  }
                  primaryText={college}
                />
              ))}
            </List>
          </Paper>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.courses.isFetching,
});

export default connect(mapStateToProps, null)(Home);

Home.propTypes = {
  isLoading: PropTypes.bool.isRequired,
};
