import React from 'react';
import LinearProgress from 'material-ui/LinearProgress';

const Linear = () =>
  (<LinearProgress
    color="orange"
    style={{ width: '250px', paddingTop: '0px', marginTop: '0px' }}
    mode="indeterminate"
  />);

const HomeLoading = () => (
  <div
    style={{
      display: 'flex',
      flexWrap: 'wrap',
      flexDirection: 'row',
      justifyContent: 'center',
      paddingTop: '100px',
      height: '95vh',
      backgroundColor: '#f5f5f5',
    }}
  >
    <div style={{ display: 'flex', justifyContent: 'center', flex: '1 0 100%' }}>
      <img
        style={{
          height: '300px',
          width: '300px',
        }}
        alt="logo"
        src={require('../../assets/whiteSquare.png')}
      />
    </div>
    <Linear style={{ flex: '1 0 100%' }} />
  </div>
);

export default HomeLoading;
