import React from 'react';
import { withRouter } from 'react-router-dom';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import Feedback from 'material-ui/svg-icons/action/bug-report';
import DisSatisfied from 'material-ui/svg-icons/social/sentiment-dissatisfied';

const styles = {
  container: {
    display: 'flex',
    position: 'relative',
    height: '110vh',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '20px',
    backgroundColor: '#111',
  },
  text: {
    fontSize: '22px',
    color: 'white',
    margin: '20px',
    marginTop: '5px',
  },
};

function ErrorBoundary(props) {
  const { history, toggleError, loggedIn } = props;
  return (
    <Paper
      zDepth={2}
      style={styles.container}
    >
      <img
        style={{ maxHeight: '250px', maxWidth: '250px', marginRight: '0 50px' }}
        alt="logo"
        src={require('../assets/blkSquare.png')}
      />
      <DisSatisfied
        style={{
          minHeight: '60px', minWidth: '60px', color: 'white',
        }}
      />
      <span style={styles.text}>
        Something went wrong !
      It maybe that the link has broken , so please retry.
      </span>
      {
        !loggedIn &&
        <div style={styles.text}>
          Looks like you are not logged in but requested a login feature , please login to fix this error.
        </div>
      }
      <p
        style={styles.text}
      >
        We are sorry for the inconvenience.
      </p>
      <br />
      <RaisedButton
        backgroundColor="white"
        label="Try Again"
        onClick={() => {
          history.push('/');
          toggleError();
        }}
        style={{ width: '142px' }}
      />
      <br />
      <a
        target="blank"
        href="https://goo.gl/forms/5dh4hdlsRLWYGXaN2"
        style={{ textDecoration: 'none' }}
      >
        <RaisedButton
          label="Bug Report"
          backgroundColor="white"
          icon={<Feedback />}
        />
      </a>
    </Paper >
  );
}

export default withRouter(ErrorBoundary);
