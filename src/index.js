import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import { PersistGate } from 'redux-persist/lib/integration/react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import routes from './config/routes';
import { store, persistor } from './redux';
import { HomeLoading } from './components/Loading';
import ErrorBoundary from './components/ErrorBoundary';

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={HomeLoading} persistor={persistor}>
    <Router>
        <ErrorBoundary>
          {renderRoutes(routes)}
        </ErrorBoundary>
      </Router>
    </PersistGate>
  </Provider>
  , document.getElementById('root'),
);
registerServiceWorker();
