import React, { Component } from 'react';
import { connect } from 'react-redux';
import Snackbar from 'material-ui/Snackbar';

import { showNotification, hideNotification } from '../../redux/actions/notification';

class Notification extends Component {
  constructor(props) {
    super(props);
  }

  handleRequestClose = () => {
    this.props.hideNotification();
  }

  render() {
    const { open, message } = this.props;
    return (
      <Snackbar
        open={open}
        message={message}
        autoHideDuration={6000}
        style={{ textAlign: 'center', width: '100%' }}
        onRequestClose={this.handleRequestClose}
      />
    );
  }
}

const mapStateToProps = state => ({
  open: state.notification.open,
  message: state.notification.message,
});

export default connect(mapStateToProps, { showNotification, hideNotification })(Notification);
