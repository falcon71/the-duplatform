import React from 'react';
import LinearProgress from 'material-ui/LinearProgress';
import { List as Loader } from 'react-content-loader';

const Linear = () => <LinearProgress color="#ec407a" mode="indeterminate" />;

const deviceHeight = (window.innerHeight - 64) / 100;
const Loading = () => (
  <div >
    <Loader style={{ minHeight: deviceHeight * 24 }} />
    <Loader style={{ minHeight: deviceHeight * 24 }} />
    <Loader style={{ minHeight: deviceHeight * 24 }} />
    <Loader style={{ minHeight: deviceHeight * 23.5 }} />
    <Linear />
  </div>
);

export default Loading;
