import React from 'react';
import LinearProgress from 'material-ui/LinearProgress';

const Linear = () =>
  (<LinearProgress
    color="orange"
    style={{ width: '250px', paddingTop: '0px', marginTop: '0px' }}
    mode="indeterminate"
  />);

const HomeLoading = () => (
  <div
    style={{
      display: 'flex',
      flexWrap: 'wrap',
      flexDirection: 'row',
      justifyContent: 'center',
      paddingTop: '100px',
      height: '95vh',
      backgroundColor: '#f5f5f5',
    }}
  >
    <img
      style={{ maxHeight: '300px', maxWidth: '300px', marginRight: '0 50px' }}
      alt="logo"
      src={require('../../assets/whiteSquare.png')}
    />
    <Linear />
  </div>
);

export default HomeLoading;
