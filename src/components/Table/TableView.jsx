import React from 'react';
import { Table } from 'reactstrap';
import Paper from 'material-ui/Paper';
import Loading from '../Loading';

function TableView({ headers, list, forCollege, isLoading }) {

  return (
    <Paper>
      <Table responsive size='lg'>
        <thead style={{ backgroundColor: '#1c2a48' }}>
          <tr>
            {
              headers.map((title, index) => (
                <th colSpan="5" style={{ color: '#fff' }} key={index}>{title}</th>
              ))
            }
          </tr>
        </thead>
        {!isLoading &&
          <tbody>
            {list.map(item => (
              <tr key={item._id}>
                {forCollege && <td colSpan='5'  >{item.collegename}</td>}
                {!forCollege && <td colSpan='5' >{item.coursename}</td>}
                <td colSpan='5'  >{item.ur}</td>
                <td colSpan='5' >{item.obc}</td>
                <td colSpan='5' >{item.sc}</td>
                <td colSpan='5' >{item.st}</td>
                <td colSpan='5' >{item.annualfee}&nbsp;&nbsp;&#8377;</td>
                <td colSpan='5' >{item.annualfeepwd}&nbsp;&nbsp;&#8377;</td>
              </tr>
            ))
            }
          </tbody>}
      </Table>
      {
        isLoading &&
        <div>
          <Loading />
        </div>
      }
    </Paper>
  );
}

export default TableView;
