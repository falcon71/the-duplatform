import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import AuthLogin from '../../views/AuthLogin';

function Auth(props) {
  const { loggedIn, location } = props;
  const redirectUrl = location.search.split('=')[1];
  return (
    loggedIn
      ? <Redirect to={redirectUrl} />
      : <AuthLogin />
  );
}

const mapStateToProps = state => ({
  loggedIn: state.auth.loggedIn,
});

export default connect(mapStateToProps, null)(Auth);

Auth.propTypes = {
  loggedIn: PropTypes.bool.isRequired,
  location: PropTypes.object.isRequired,
};
