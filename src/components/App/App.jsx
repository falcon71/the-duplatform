import React, { Component } from 'react';
import { connect } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import Notification from '../Notification';
import { fetchColleges } from '../../redux/actions/colleges';
import { fetchCourses } from '../../redux/actions/courses';
import { fetchCourseList } from '../../redux/actions/courseList';

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: '#01579B',
  },
});

class App extends Component {
  componentDidMount() {
    // fetch colleges and add it to store
    this.props.fetchColleges();
    this.props.fetchCourses();
    this.props.fetchCourseList();
  }

  render() {
    const { route } = this.props;
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <div>
          {renderRoutes(route.routes)}
          <Notification />
        </div>
      </MuiThemeProvider>
    );
  }
}

export default connect(null, { fetchColleges, fetchCourses, fetchCourseList })(App);
