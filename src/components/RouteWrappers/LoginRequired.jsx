import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import PropTypes from 'prop-types';
import { endPoints } from '../../config/routes';
import Loading from '../Loading';

function LoginRequired(props) {
  const {
    route,
    loggedIn,
    location,
  } = props;
  const path = location.pathname;
  return (
    <div>
      {loggedIn && renderRoutes(route.routes)}
      {!loggedIn && <Redirect to={`${endPoints.login}?redirect=${path}`} />}
    </div>
  );
}

const mapStateToProps = state => ({
  loggedIn: state.auth.loggedIn,
});

export default connect(mapStateToProps, null)(LoginRequired);

LoginRequired.propTypes = {
  route: PropTypes.object.isRequired,
  loggedIn: PropTypes.bool.isRequired,
  location: PropTypes.object.isRequired,
};
