import React, { Component } from 'react';

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch() {
    this.setState({ hasError: true });
  }

  render() {
    const { hasError } = this.state;
    return (
      hasError
        ? <ErrorMessage />
        : this.props.children
    );
  }
}


export default ErrorBoundary;

function ErrorMessage() {
  return (
    <div>
      Something went wrong!
    </div>
  );
}
