export const HOME = "/";
export const ME = "/me";
export const SIGN_IN = "/login";
export const SIGN_UP = "/signup";
export const WHY_DU = "/whydu";
export const COURSES = "/courses";
export const COLLEGES = "/colleges";
export const CONTRIBUTE = "/contribute";
export const RESOURCES = "/resources";
export const RESOURCES_PATH = "/resources/:path";

export const ADMISSION = "/admission";

export const COLLEGE_DETAIL = "/college/detail";
export const COLLEGE_DETAILS = "/college/details/";
export const COLLEGE_DETAILS_ID = "/college/details/:id";

export const ASSESTS = "../assests/";
export const COURSE_DETAIL = "/course/detail/";
export const COURSE_DETAILS = "/course/details/";
export const COURSE_DETAILS_ID = "/course/details/:id";
