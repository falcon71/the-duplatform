import routes from './routes';

import endPoints from './endpoints';

export {
  endPoints,
};

export default routes;
