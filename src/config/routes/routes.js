import App from '../../components/App';
import Auth from '../../components/Auth';

import { LoginRequired } from '../../components/RouteWrappers';
import endPoints from './endpoints';
import { HeaderWithDrawer } from '../../views/Layouts';
import {
  WhyDu,
  Admission,
} from '../../views/Static';
import {
  CourseList,
  CourseDetails,
} from '../../views/Courses';
import Home from '../../views/Home';
import {
  CollegeList,
  CollegeDetails,
} from '../../views/Colleges';
import {
  Resources,
  DownloadList,
} from '../../views/Resources';
import Contribute from '../../views/Contribute';


export default [
  {
    component: App, // Root
    routes: [
      {
        path: endPoints.login,
        component: HeaderWithDrawer,
        routes: [
          {
            path: endPoints.login,
            exact: true,
            component: Auth, // View component
          },
        ],
      },
      {
        component: LoginRequired, // Route Protector
        routes: [
          {
            component: HeaderWithDrawer,
            routes: [
              {
                path: endPoints.default,
                exact: true,
                component: Home, // View component
              },
              {
                path: endPoints.whydu,
                exact: true,
                component: WhyDu, // View component
              },
              {
                path: endPoints.admission,
                exact: true,
                component: Admission, // View component
              },
              {
                path: endPoints.colleges,
                exact: true,
                component: CollegeList, // View component
              },
              {
                path: endPoints.collegeDetailsById,
                component: CollegeDetails, // View component
              },
              {
                path: endPoints.courses,
                exact: true,
                component: CourseList, // View component
              },
              {
                path: endPoints.courseDetailsById,
                component: CourseDetails, // View component
              },
              {
                path: endPoints.contribute,
                exact: true,
                component: Contribute, // View component
              },
              {
                path: endPoints.resources,
                exact: true,
                component: Resources, // View component
              },
              {
                path: endPoints.resourcesByPath,
                component: DownloadList,
              },
            ],
          },
        ],
      },
    ],
  },
];
