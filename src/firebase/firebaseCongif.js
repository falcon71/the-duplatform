import * as firebase from 'firebase';

// Initialize Firebase
const config = {
  apiKey: "AIzaSyD8B6RjUPR76LxaQLo-JRv_tbRJcSIdYZU",
  authDomain: "falcon-d4243.firebaseapp.com",
  databaseURL: "https://falcon-d4243.firebaseio.com",
  projectId: "falcon-d4243",
  storageBucket: "falcon-d4243.appspot.com",
  messagingSenderId: "22720676053"
};

firebase.initializeApp(config);

export const auth = firebase.auth();
